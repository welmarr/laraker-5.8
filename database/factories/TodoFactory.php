<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Todo;
use Faker\Generator as Faker;

$factory->define(Todo::class, function (Faker $faker) {
	$text = $faker->sentence();
    return [
        'text' => $text,
        'text_crypt' => $text,
        'completed' => $faker->boolean(),
        'user_id' => random_int(1, 10),
    ];
});
